const db = require("../db");

function generatePagination(data, page, pages, count) {
    var pagination = {
        data,
        page,
        next: Math.min(page + 1, pages),
        prev: Math.max(1, page - 1),
        last: pages,
        first: 1,
        count
    };


    if (pagination.last == page) {
        pagination.next = null;
    }

    if (pagination.first == page) {
        pagination.prev = null;
    }




    var pageNumbers = [];

    var pageNumberInitial = Math.max(1, page - 4);
    var pageNumberLast = Math.min(page + 4, pages);

    var lastDiff = 4 - (pageNumberLast - page)
    var firstDiff = 4 - (page - pageNumberInitial);

    if (lastDiff > 0) {
        pageNumberInitial -= lastDiff;
        var pageNumberInitial = Math.max(1, pageNumberInitial);
    } else if (firstDiff > 0) {
        pageNumberLast += firstDiff;
        var pageNumberLast = Math.min(pageNumberLast, pages);
    }



    for (var i = pageNumberInitial; i <= pageNumberLast; i++) {
        pageNumbers.push(i);
    }

    pagination.numbers = pageNumbers;

    if (~pagination.numbers.indexOf(pagination.first)) {
        pagination.first = null;
    }

    if (~pagination.numbers.indexOf(pagination.last)) {
        pagination.last = null;
    }

    return pagination;

}


function searchBattles(where, pageId, reply, query) {

    const itensPerPage = 20;

    db.battles.count(where).exec(function (err, count) {
        const pages = Math.ceil(count / itensPerPage);
        pageId = Math.min(pages, pageId);

        var skip = itensPerPage * (pageId - 1);

        db.battles.find(where).skip(skip).limit(itensPerPage).exec(function (err, docs) {

            if (err) return reply(err);

            const pagination = generatePagination(docs, pageId, pages, count);

            pagination.query = query;

            reply(null, pagination);
        });

    });
}


function battlesController(server) {

    server.route({
        method: "GET",
        path: '/battle/recents',
        handler: function (request, reply) {

            db.battles.find({}).sort({ date: -1 }).limit(8).exec(function (err, docs) {
                reply(err || docs);
            });

        }
    });

    server.route({
        method: "GET",
        path: '/battle/teams',
        handler: function (request, reply) {

            db.heroes.find({}).projection({ team: 1 }).exec(function (err, docs) {

                var teams = [];

                if (docs) {

                    teams = docs.reduce((g, i) => {

                        var team = i.team;
                        if (team) {
                            if (!~g.indexOf(team)) g.push(team);
                        }
                        return g;
                    }, [])

                }

                reply(err || teams.sort());

            })

        }
    })

    server.route({
        method: "GET",
        path: '/battle/page/{pageId}',
        handler: function (request, reply) {

            var pageId = request.params.pageId || 1;

            var where = {};
            var query = request.query;

            if ("hero" in request.query && request.query.hero) {
                where.heroName = new RegExp(".*" + request.query.hero + ".*", 'i');
            }

            if ("team" in request.query && request.query.team) {

                db.heroes.find({ team: request.query.team }, function (err, heroes) {

                    where.heroId = { $in: heroes.map(x => x.heroId) };


                    searchBattles(where, pageId, function (err, res) {
                        reply(err || res);
                    }, query);

                });


            } else {
                searchBattles(where, pageId, function (err, res) {
                    reply(err || res);
                }, query);
            }


        }
    });


}

module.exports = battlesController;