const db = require("../db");
const moment = require("moment");


function heroesController(server) {

    server.route({
        method: 'GET',
        path: '/heroes',
        handler: function (request, reply) {

            const year = moment().year();
            const now = moment();

            db.heroes.find({}, function (err, docs) {
                reply(err || docs.map(hero => {

                    delete hero.image;
                    delete hero.imageUrl;

                    hero.birthday = moment(hero.birthdate);
                    hero.birthday.year(year);
                    if (hero.birthday.isBefore(now)) {
                        hero.birthday.year(year + 1);
                    }
                    hero.birthdayText = hero.birthday.fromNow();
                    hero.birthday = hero.birthday.toDate();

                    return hero;

                }));
            });

        }
    });

    server.route({
        method: 'GET',
        path: '/hero/{id}/avatar',
        handler: function (request, reply) {
            var id = request.params.id;

            db.heroes.findOne({ heroId: id }, function (err, doc) {

                if (err) reply(err);

                reply.file(doc.imageUrl);

            });

        }
    });

}

module.exports = heroesController;