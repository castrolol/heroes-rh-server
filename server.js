const Hapi = require('hapi');
const Inert = require("inert");

const server = new Hapi.Server();

server.connection({
    host: 'localhost',
    port: 8000,
    routes: { cors: true }
});

server.register(Inert, () => { });



function startServer(initControllers) {

    initControllers(server);


    server.start((err) => {

        if (err) {
            throw err;
        }
        console.log('Server running at:', server.info.uri);
    });
 
};


module.exports = startServer;