const startServer = require("./server");

const heroesController = require("./controllers/heroes-controller");
const battlesController = require("./controllers/battles-controller");



startServer(function(server){

    heroesController(server);
    battlesController(server);

});