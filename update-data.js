const db = require("./db");
const faker = require("faker");


db.heroes.find({}, function (err, docs) {

    var i = 0;

    (function step() {
        
        if (i < docs.length ) {
            const hero = docs[i];
            i++;

            hero.birthdate = faker.date.past(32, new Date(1993, 1,1));

            console.log("updating... " + hero.name);
            db.heroes.update({ heroId: hero.heroId }, hero, step);
        }

    }());

});