const db = require("./db");
const faker = require("faker");


var heroes = [];

var types = [
    "Against darkness and caos",
    "Prevent world domination",
    "Saving people from bridge collapse",
    "Biological control bomb",
    "Investigating kidnapping and rape of presidents",
    "Alien invasion and abduction",
    "Gang meta-human",
    "Russian nuclear attack",
    "Break Nazi secret base",
    "breaking laboratory to create aberrations",
    "Recover city of the future villains"
];

var durations = [
    "day(s)",
    "hour(s)",
    "week(s)",
];

var status = [
    "paid out",
    "payable",
    "granted",
    "splitted"
];

function createBattle() {

    var battle = {};
    var hero = faker.helpers.randomize(heroes);

    battle.heroId = hero.heroId;
    battle.heroName = hero.name;
    battle.date = faker.date.past();
    battle.cost = faker.finance.amount(400, 1500);
    battle.type = faker.helpers.randomize(types);
    battle.location = {
        name: faker.address.streetAddress() + ", " + faker.address.city() + " - " + faker.address.state(),
        lat: faker.address.latitude(),
        lon: faker.address.longitude(),
    };
    battle.status = faker.helpers.randomize(status); 
    battle.duration = {
        quantity: faker.random.number({ min: 1, max: 7 }),
        type: faker.helpers.randomize(durations)
    };
    battle.deadAmount = Math.round(Math.log(faker.random.number({ min: 0, max: 1000000 })));

    return battle;

}


function genBattles() {
    var battles = [];

    for (var i = 0; i < 50000; i++) {

        var battle = createBattle();
        battles.push(battle);
    }

    db.battles.insert(battles, function(err){

        if(err) console.log("Error", err);
        console.log("finished");

    });


}

(function init() {

    db.heroes.find({}, (err, docs) => {

        heroes = docs;

        genBattles();

    });

}());