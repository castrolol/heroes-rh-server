const Datastore = require('nedb');
const db = {
    heroes: new Datastore({ filename: './data/heroes.db', autoload: true }),
    payments: new Datastore({ filename: './data/payments.db', autoload: true }),
    battles: new Datastore({ filename: './data/battles.db', autoload: true }),
};



module.exports = db;