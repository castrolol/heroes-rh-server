const db = require("./db");
const faker = require("faker");
var http = require('http');
var https = require('https');
var fs = require('fs');
var path = require("path");
var request = require("request");

function downloadImage(hero, cb) {

    var r = http;
    var img = hero.image;

    if (~img.indexOf("https://")) {
        r = https;
    }

    var name = path.basename(img);
    hero.imageUrl = "./data/images/" + name;

    request({
        'url': img,
        'proxy': 'http://ps-luan:ak47m4a1@proxy.pjmt.local:3128'
    },cb)
    .pipe(fs.createWriteStream(hero.imageUrl))



}

function getEmail(hero) {
    var names = ~hero.name.indexOf(" ") ? hero.name.split(" ") : ["super", "hero", hero.name];

    var first = faker.random.arrayElement(names);
    var index = names.indexOf(first);
    names.splice(index, 1);
    var second = faker.random.arrayElement(names);
    var provider = faker.random.arrayElement(["marvel", "heroic", "hmail", "herolook"]) + ".com";

    return faker.internet.email(first, second, provider);


}

function insertHero(hero, next) {

    var hero = Object.assign({}, hero);

    hero.heroId = faker.random.uuid();
    hero.salary = +faker.finance.amount(800, 25000, 2);
    hero.phone = faker.phone.phoneNumber("(##) 9 ####-####");
    hero.email = getEmail(hero);
 hero.birthdate = faker.date.past(32, new Date(1993, 1,1));

    console.log("inserindo");
    console.log(hero);
    console.log("_--_");

    downloadImage(hero, () => {
        db.heroes.insert(hero, next);
    });


}

const heroes = require("./static/heroes.json");


var i = 0;

function step() {
    if (i >= heroes.length) return;

    var h = heroes[i];
    i++;
    insertHero(h, step);
}


step();
